import { Button, Flex, Heading, Text } from "@chakra-ui/react";
import { motion, useIsPresent } from "framer-motion";
import { AnimatePresence } from "framer-motion";
import * as React from "react";
import { Outlet, NavLink, useLocation, useRoutes } from "react-router-dom";

const ROUTES = {
  INDEX: {
    ID: "home",
    NAME: "Home",
    PATH: "/",
    BG_ACTIVE: "linear(to-tr, #7928CA, #FF0080)",
    BG_HOVER: "linear(to-tr, blue.500, cyan.500)",
    BG: {
      background: "linear-gradient(135deg, #7928CA, #FF0080)",
    },
  },
  ABOUT: {
    ID: "about",
    NAME: "About",
    PATH: "/about",
    BG_ACTIVE: "linear(to-tr, yellow, red)",
    BG_HOVER: "linear(to-tr, red.500, yellow.500)",
    BG: {
      background: "linear-gradient(135deg, yellow, red)",
    },
  },
  DASHBOARD: {
    ID: "dashboard",
    NAME: "Dashboard",
    PATH: "/dashboard",
    BG_ACTIVE: "linear(to-bl, green, blue)",
    BG_HOVER: "linear(to-tr, green.500, green)",
    BG: {
      background: "linear-gradient(135deg, green, blue)",
    },
  },
  NOTHING_HERE: {
    ID: "nothing-here",
    NAME: "Nothing Here",
    PATH: "/nothing-here",
    BG_ACTIVE: "linear(to-bl, green, blue)",
    BG_HOVER: "linear(to-tr, yellow, yellow.300)",
    BG: {
      background: "linear-gradient(135deg, black, white)",
    },
  },
  NO_MATCH: {
    ID: "no_match",
    NAME: "NO MATCH",
    PATH: "*",
    BG_ACTIVE: "linear(to-tr, black, white, white, black)",
    BG: {
      background: "linear-gradient(135deg, black, white, white, black)",
    },
  },
};
export default function App() {
  const location = useLocation();

  const element = useRoutes([
    {
      path: ROUTES.INDEX.PATH,
      element: <Layout />,
      children: [
        {
          index: true,
          element: <Home />,
        },
        {
          path: ROUTES.ABOUT.PATH,
          element: <About />,
        },
        {
          path: ROUTES.DASHBOARD.PATH,
          element: <Dashboard />,
        },
        {
          path: ROUTES.NO_MATCH.PATH,
          element: <NoMatch />,
        },
      ],
    },
  ]);

  if (!element) return null;

  return (
    <Flex className="app" position="relative" h="100vh" w="100vw">
      <AnimatePresence mode="wait" initial={false}>
        {React.cloneElement(element, { key: location.pathname })}
      </AnimatePresence>
    </Flex>
  );
}

function Layout() {
  const isPresent = useIsPresent();

  const handleLinkStyle = (id, { isActive }) => {
    let styles = {};

    if (isActive) {
      styles = {
        ...ROUTES[id].BG,
      };
    }
    // if (id === ROUTES.INDEX.ID) {
    //   styles = {
    //     background: isActive && ROUTES.INDEX.BG_ACTIVE,
    //   };
    // } else if (id === ROUTES.ABOUT.ID) {
    //   styles = {
    //     background: isActive && ROUTES.ABOUT.BG_ACTIVE,
    //   };
    // } else if (id === ROUTES.DASHBOARD.ID) {
    //   styles = {
    //     background: isActive && ROUTES.DASHBOARD.BG_ACTIVE,
    //   };
    // }

    return {
      color: isActive && "white",
      ...styles,
    };
  };

  return (
    <Flex className="layout" h="100vh" w="100vw">
      {/* A "layout route" is a good place to put markup you want to
          share across all the pages on your site, like navigation. */}
      <Flex
        p={6}
        direction="column"
        borderRight="1px solid #ddd"
        position="fixed"
        h="100vh"
        gap="15px"
        w={1 / 5}
        className="menu-nav"
      >
        {Object.keys(ROUTES).map((route) => {
          if (route === ROUTES.NO_MATCH.ID.toUpperCase()) return null;

          return (
            <Button
              key={route}
              as={NavLink}
              to={ROUTES[route].PATH}
              style={(styles) => handleLinkStyle(route, styles)}
              _hover={{
                bgGradient: ROUTES[route].BG_HOVER,
              }}
            >
              {ROUTES[route].NAME}
            </Button>
          );
        })}
      </Flex>

      {/* An <Outlet> renders whatever child route is currently active,
          so you can think about this <Outlet> as a placeholder for
          the child routes we defined above. */}
      <Flex
        className="outlet"
        ml="20%"
        p={6}
        w="100%"
        as={motion.article}
        initial={{ opacity: 0 }}
        animate={{
          opacity: 1,
          transition: { duration: 2, ease: "circIn" },
        }}
        layout
      >
        <Outlet />
      </Flex>

      {/* A <motion.element> will be used for the transition animation */}
      <motion.div
        initial={{ scaleX: 1, backgroundColor: "rgba(0,107,207,1)" }}
        animate={{
          scaleX: 0,
          transition: { duration: 0.3, ease: "circIn" },
          backgroundColor: "rgba(0,107,207,1)",
        }}
        exit={{
          scaleX: 1,
          transition: { duration: 0.3, ease: "circIn" },
          backgroundColor: "rgba(0,107,207,1)",
        }}
        style={{
          originX: isPresent ? 0 : 1,
          position: "fixed",
          top: "0",
          left: "0",
          bottom: "0",
          right: "0",
          zIndex: "2",
        }}
        className="motion-object"
      />
    </Flex>
  );
}

function Home() {
  return (
    <Flex
      bgGradient={ROUTES.INDEX.BG_ACTIVE}
      w="100%"
      justify="center"
      align="center"
    >
      <Heading size="4xl">Home</Heading>
    </Flex>
  );
}

function About() {
  return (
    <Flex
      bgGradient={ROUTES.ABOUT.BG_ACTIVE}
      w="100%"
      justify="center"
      align="center"
    >
      <Heading size="4xl">About</Heading>
    </Flex>
  );
}

function Dashboard() {
  return (
    <Flex
      bgGradient={ROUTES.DASHBOARD.BG_ACTIVE}
      w="100%"
      justify="center"
      align="center"
    >
      <Heading size="4xl">Dashboard</Heading>
    </Flex>
  );
}

function NoMatch() {
  return (
    <Flex
      direction="column"
      bgGradient={ROUTES.NO_MATCH.BG_ACTIVE}
      w="100%"
      justify="center"
      align="center"
    >
      <Heading size="4xl">Nothing to see here!</Heading>
      <Text mt="20px" fontSize="3xl">
        <Button variant="link">
          <NavLink to="/">Go to the home page</NavLink>
        </Button>
      </Text>
    </Flex>
  );
}
